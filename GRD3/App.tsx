import React from 'react';
import { View, StyleSheet, SafeAreaView, Image, Platform, ScrollView, Text, TextInput } from 'react-native';
import icPesan1 from './src/icons/pesan1.png'
import icPesan2 from './src/icons/pesan2.png'
import icScan from './src/icons/scan.png'
import icSearch from './src/icons/search.png'
import icBalance from './src/icons/balance.png'
import icCrown from './src/icons/crown.png'
import icFood from './src/icons/food.png'
import icBike from './src/icons/bike.png'
import icCar from './src/icons/car.png'
import icBox from './src/icons/box.png'
import icMart from './src/icons/mart.png'
import icPulsa from './src/icons/puls.png'
import icJastip from './src/icons/jastip.png'
import icLove from './src/icons/love.png'



const App = () => {
  return (
    <SafeAreaView style={styles.container}>
      <View>
        <View style={styles.box}>
          <View style={{
            flexDirection:'row',
            justifyContent:'space-between',
            
          }}>
            <View style={styles.scan}>
              <Image source={icScan}
                style={{
                  width:30,
                  height:30,
                  resizeMode:'contain',
                  alignSelf:'center',
                }}/>
            </View>
            <View style={styles.search}>
               <Image source={icSearch}
                  style={{
                    width:28,
                    height:28,
                    resizeMode:'contain',
                    marginLeft:-10,
                    marginTop:-8
                  }}/>
                  <TextInput 
                    placeholder='makan'
                    style={{
                      alignContent:'center',
                      marginLeft:4,
                      height:10
                    }}
                  />
            </View>
            <View style={styles.love}>
                <Image source={icLove}
                  style={{
                    width:30,
                    height:30,
                    resizeMode:'contain',
                    alignSelf:'center'
                  }}/>
            </View>
          </View>
        </View>
      </View>
      
      <View>
        {/* MENU */}
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>

        {/* MENU 1 */}
        <View style={{
          // flex:1,
          backgroundColor : 'white',
          flexDirection:'row',
          justifyContent:'space-between'
        }}> 
        <View>
          <View style={styles.Food}>
            <Image source={icFood}
              style={{
                width:34,
                height:34,
                resizeMode:'contain',
               alignSelf:'center'
              }}/>
          </View>
          <Text style={{
            textAlign:'center'
          }}>Makanan</Text>
        </View>
          {/* MENU 2 */}
          <View>

          <View
            style={styles.Bike}>
            <Image source={icBike}
              style={{
                width:34,
                height:34,
                resizeMode:'contain',
                alignSelf:'center'
              }}/>
          </View>
          <Text style={{
            textAlign:'center'
          }}>Motor</Text>
          </View>
          <View>

          <View
            style={styles.Car}>
            <Image source={icCar}
              style={{
                width:37,
                height:37,
                resizeMode:'contain',
                alignSelf:'center'
              }}/>
          </View>
          <Text style={{
            textAlign:'center'
          }}>Mobil</Text>
          </View>
          <View>

          <View
            style={styles.Box}>
            <Image source={icBox}
              style={{
                width:34,
                height:34,
                resizeMode:'contain',
                alignSelf:'center'
              }}/>
          </View>
          <Text style={{
            textAlign:'center'
          }}>Box</Text>
          </View>
          <View>

          <View
            style={styles.Mart}>
            <Image source={icMart}
              style={{
                width:34,
                height:34,
                resizeMode:'contain',
                alignSelf:'center'
              }}/>
          </View>
          <Text style={{
            textAlign:'center'
          }}>Mart</Text>
          </View>
          <View>
          <View
            style={styles.Pulsa}>
            <Image source={icPulsa}
              style={{
                width:34,
                height:34,
                resizeMode:'contain',
                alignSelf:'center'
              }}/>
          </View>
          <Text style={{
            textAlign:'center'
          }}>Pulsa</Text>
          </View>

          <View>
          <View
            style={styles.Pulsa}>
            <Image source={icJastip}
              style={{
                width:34,
                height:34,
                resizeMode:'contain',
                alignSelf:'center'
              }}/>
          </View>
          <Text style={{
            textAlign:'center'
          }}>Jastip</Text>
          </View>
          
          <View>

        </View>
        </View>
      </ScrollView>
    {/* BALANCE */}
    <View 
      style={{
        flexDirection:'row',
        justifyContent:'space-between',
        backgroundColor:'white'
      }}>
        <View style={styles.balance}>
          <View
            style={{
              justifyContent:'center',
              alignContent:'center'
            }}>
              <Text style={{color:'black'}}>
                Balance
              </Text>
              <Text style={{fontWeight:'bold', color:'black'}}>
                Rp 2.673
              </Text>
          </View>
          <Image 
            source={icBalance}
            style={{
              width:30,
              height:30,
              resizeMode:'contain',
              alignSelf:'center'
              }}/>
        </View>

        <View style={styles.balance}>
          <View
            style={{
              justifyContent:'center',
              alignContent:'center'
            }}>
              <Text style={{color:'black'}}>
                Use Points
              </Text>
              <Text style={{fontWeight:'bold', color:'black'}}>
                112
              </Text>
          </View>
          <Image 
            source={icCrown}
            style={{
              width:30,
              height:30,
              resizeMode:'contain',
              alignSelf:'center'
              }}/>
              
        </View>
    </View>
{/* PESAN SEKARANG */}
        <View style={{
          backgroundColor:'white',
          }}>
          <Text style={{
            color:'black',
            marginLeft:10,
            marginTop:20,
            fontSize:19,
            fontWeight:'bold'
          }}>Pesan Sekarang</Text>

          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={{
              flexDirection:'row'
              }}>
              <View style={{
                height:250,
                width:Platform.OS == 'android' ? 390 : 370,
                backgroundColor:'white',
                borderRadius:10,
                margin:10,
                justifyContent:'center',
                alignContent:'center'
              }}>
                <Image source={icPesan1}
                style = {{
                  height:240,
                width:Platform.OS == 'android' ? 380 : 360,
                }}/>
              </View>

              <View style={{
                height:250,
                width:Platform.OS == 'android' ? 390 : 370,
               
                backgroundColor:'white',
                
                borderRadius:10,
                margin:10,
                justifyContent:'center',
                alignContent:'center'
              }}>
                <Image source={icPesan2}
                style = {{
                  height:240,
                width:Platform.OS == 'android' ? 380 : 360,
                }}/>
              </View>
            </View>
          </ScrollView>
          <Text style={{
            fontWeight:'bold',
            color:'black',
            fontSize:16,
            marginLeft:10
          }}>Beli ISDU Diskon 100%</Text>
          <Text style={{
            marginLeft:10,
            color:'black',
          }}>Sponsored by HaloAcademy</Text>
        <View>
          <Text style={{
            fontWeight:'bold',
            marginLeft:10,
            marginTop:25,
            fontSize:15,
            color:'black',
            top: Platform.OS == 'android'?-20:80,
          }}>Order food again</Text>

        </View>
        </View>

    </View>
  </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor :'#33c072',
    // alignContent: 'center',
    // justifyContent:'center',
    // alignItems:'center'
  },
  box: {
    width: 0.1,
    height: 50,
    backgroundColor: 'white',
    top :50,
    bottom :50,
    marginBottom:45
    
  },
  scan: {
    padding : 20,
    marginTop:-23,
    margin:10,
    width : 50,
    height: 50,
    borderRadius : 7,
    backgroundColor:'white',
    borderColor:'black',
    borderWidth:0.3,
    justifyContent:'center',
    alignContent:'center',
  },
  search: {
    padding : 20,
    marginTop:-23,
    margin:10,
    width:Platform.OS == 'ios' ? 250 : 270,
    height: 50,
    borderTopLeftRadius:7,
    borderBottomLeftRadius:7,
    backgroundColor:'white',
    borderColor:'black',
    borderWidth:0.3,
    // justifyContent:'center',
    alignContent:'center',
    flexDirection:'row',
  },
  love: {
    marginTop:-23,
    width : 50,
    height: 50,
    borderTopRightRadius:7,
    borderBottomRightRadius:7,
    backgroundColor:'white',
    borderColor:'black',
    // borderWidth:0.3,
    justifyContent:'center',
    alignContent:'center',
    marginLeft:-10,
    borderRightWidth:0.3,
    borderBottomWidth:0.3,
    borderTopWidth:0.3

  },
  Food:{
    marginTop:20,
    width : 50,
    height: 50,
    backgroundColor:'#c8f0dd',
    borderColor:'#c8f0dd',
    borderWidth:1,
    justifyContent:'center',
    alignContent:'center',
    marginLeft:10,
    marginRight:10,
    borderRadius:100,
  },
  Bike:{
    marginTop:20,
    width : 50,
    height: 50,
    backgroundColor:'#c8f0dd',
    borderColor:'#c8f0dd',
    borderWidth:1,
    justifyContent:'center',
    alignContent:'center',
    marginLeft:10,
    marginRight:10,
    borderRadius:100,
  },
  Car:{
    marginTop:20,
    width : 50,
    height: 50,
    backgroundColor:'#c8f0dd',
    borderColor:'#c8f0dd',
    borderWidth:1,
    justifyContent:'center',
    alignContent:'center',
    marginLeft:10,
    marginRight:10,
    borderRadius:100,
  },
  Box:{
    marginTop:20,
    width : 50,
    height: 50,
    backgroundColor:'#c8f0dd',
    borderColor:'#c8f0dd',
    borderWidth:1,
    justifyContent:'center',
    alignContent:'center',
    marginLeft:10,
    marginRight:10,
    borderRadius:100,
  },
  Mart:{
    marginTop:20,
    width : 50,
    height: 50,
    backgroundColor:'#c8f0dd',
    borderColor:'#c8f0dd',
    borderWidth:1,
    justifyContent:'center',
    alignContent:'center',
    marginLeft:10,
    marginRight:10,
    borderRadius:100,
  },
  Pulsa:{
    marginTop:20,
    width : 50,
    height: 50,
    backgroundColor:'#c8f0dd',
    borderColor:'#c8f0dd',
    borderWidth:1,
    justifyContent:'center',
    alignContent:'center',
    marginLeft:10,
    marginRight:10,
    borderRadius:100,
  },
  
  balance: {
    marginTop:40,
    width : Platform.OS == 'android' ? 185 : 175,
    height: 50,
    borderWidth:1,
    backgroundColor:'#e6e6e6',
    borderColor:'#e6e6e6',
    // borderWidth:0.3,
    justifyContent:'space-between',
    alignContent:'center',
    marginLeft:10,
    marginRight: 10,
    borderRadius:7,
    flexDirection:'row',
    padding:10
  },
});

export default App;